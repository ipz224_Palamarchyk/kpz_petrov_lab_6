﻿using Checkers.Commands;
using Checkers.Models;
using Checkers.Services;
using System.Windows.Input;

namespace Checkers.ViewModels
{
    public class GameSquareVM : BaseNotification
    {
        private readonly GameLogic gameLogic;
        private GameSquare square;

        public GameSquareVM(GameSquare square, GameLogic gameLogic)
        {
            this.square = square;
            this.gameLogic = gameLogic;
            ClickPieceCommand = new RelayCommand<GameSquare>(gameLogic.ClickPiece);
            MovePieceCommand = new RelayCommand<GameSquare>(gameLogic.MovePiece);
        }

        public GameSquare Square
        {
            get => square;
            set
            {
                square = value;
                NotifyPropertyChanged(nameof(Square));
            }
        }

        public ICommand ClickPieceCommand { get; }
        public ICommand MovePieceCommand { get; }
    }
}
