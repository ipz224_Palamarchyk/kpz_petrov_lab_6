﻿using Checkers.Commands;
using Checkers.Models;
using Checkers.Services;
using System.Windows.Input;

namespace Checkers.ViewModels
{
    public class ButtonInteractionVM : BaseNotification
    {
        private readonly GameLogic gameLogic;
        private ICommand resetCommand;
        private ICommand saveCommand;
        private ICommand loadCommand;

        public ButtonInteractionVM(GameLogic gameLogic)
        {
            this.gameLogic = gameLogic;
        }

        public ICommand ResetGameCommand
        {
            get
            {
                if (resetCommand == null)
                {
                    resetCommand = new NonGenericCommand(gameLogic.ResetGame);
                }
                return resetCommand;
            }
        }

        public ICommand SaveGameCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new NonGenericCommand(gameLogic.SaveGame);
                }
                return saveCommand;
            }
        }

        public ICommand LoadGameCommand
        {
            get
            {
                if (loadCommand == null)
                {
                    loadCommand = new NonGenericCommand(gameLogic.LoadGame);
                }
                return loadCommand;
            }
        }
    }
}
