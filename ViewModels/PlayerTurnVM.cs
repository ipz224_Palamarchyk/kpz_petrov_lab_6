﻿using Checkers.Models;
using Checkers.Services;

namespace Checkers.ViewModels
{
    public class PlayerTurnViewModel : BaseNotification
    {
        private readonly GameLogic _gameLogic;
        private PlayerTurn _playerTurn;

        public PlayerTurnViewModel(GameLogic gameLogic, PlayerTurn playerTurn)
        {
            _gameLogic = gameLogic;
            PlayerTurn = playerTurn;
        }

        public PlayerTurn PlayerTurn
        {
            get { return _playerTurn; }
            set
            {
                _playerTurn = value;
                NotifyPropertyChanged(nameof(PlayerTurn));
            }
        }
    }
}
