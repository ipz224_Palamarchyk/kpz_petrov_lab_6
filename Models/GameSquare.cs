﻿using Checkers.Enums;
using Checkers.Services;
using System.ComponentModel;

namespace Checkers.Models
{
    public class GameSquare : INotifyPropertyChanged
    {
        private int _row;
        private int _column;
        private SquareShade _shade;
        private string _texture;
        private GamePiece? _piece;
        private string? _legalSquareSymbol;

        public event PropertyChangedEventHandler? PropertyChanged;

        public GameSquare(int row, int column, SquareShade shade, GamePiece piece)
        {
            _row = row;
            _column = column;
            _shade = shade;
            _texture = shade == SquareShade.Dark ? Utility.redSquare : Utility.whiteSquare;
            _piece = piece;
        }

        public int Row => _row;

        public int Column => _column;

        public SquareShade Shade => _shade;

        public string Texture
        {
            get => _texture;
            set
            {
                _texture = value;
                NotifyPropertyChanged(nameof(Texture));
            }
        }

        public GamePiece? Piece
        {
            get => _piece;
            set
            {
                _piece = value;
                NotifyPropertyChanged(nameof(Piece));
            }
        }

        public string? LegalSquareSymbol
        {
            get => _legalSquareSymbol;
            set
            {
                _legalSquareSymbol = value;
                NotifyPropertyChanged(nameof(LegalSquareSymbol));
            }
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
