﻿using Checkers.Enums;
using Checkers.Services;
using System.ComponentModel;

namespace Checkers.Models
{
    public class GamePiece : INotifyPropertyChanged
    {
        private readonly PieceColor _color;
        private PieceType _type;
        private string _texture;
        private GameSquare? _square;

        public event PropertyChangedEventHandler? PropertyChanged;

        public GamePiece(PieceColor color)
        {
            _color = color;
            _type = PieceType.Regular;
            _texture = _color == PieceColor.Red ? Utility.redPiece : Utility.whitePiece;
        }

        public GamePiece(PieceColor color, PieceType type)
        {
            _color = color;
            _type = type;
            _texture = _color == PieceColor.Red ? Utility.redPiece : Utility.whitePiece;

            if (_type == PieceType.King)
            {
                _texture = _color == PieceColor.Red ? Utility.redPiece : Utility.whitePiece;
            }
        }

        public PieceColor Color => _color;

        public PieceType Type
        {
            get => _type;
            set
            {
                _type = value;
                NotifyPropertyChanged(nameof(Type));
            }
        }

        public string Texture
        {
            get => _texture;
            set
            {
                _texture = value;
                NotifyPropertyChanged(nameof(Texture));
            }
        }

        public GameSquare? Square
        {
            get => _square;
            set
            {
                _square = value;
                NotifyPropertyChanged(nameof(Square));
            }
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
