﻿using Checkers.Enums;
using Checkers.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Services
{
    public static class GameLoader
    {

        private static GameState _gameState;

        public static void LoadGame(ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            _gameState = new GameState();

            OpenFileDialog openDialog = new OpenFileDialog();
            bool? answer = openDialog.ShowDialog();

            if (answer == true)
            {
                string path = openDialog.FileName;
                using (var reader = new StreamReader(path))
                {
                    LoadCurrentPieceLocation(reader, squares);
                    LoadGameSettings(reader);
                    LoadTurn(reader);
                    LoadBoard(reader, squares);
                    LoadCurrentNeighbours(reader, squares);
                }
            }
        }

        private static void LoadCurrentPieceLocation(StreamReader reader, ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            string text = reader.ReadLine();
            ResetCurrentSquare();

            if (text != GameConstants.NO_PIECE.ToString())
            {
                int row = (int)char.GetNumericValue(text[0]);
                int col = (int)char.GetNumericValue(text[1]);
                _gameState.CurrentSquare = squares[row][col];
                _gameState.CurrentSquare.Texture = GameConstants.SQUARE_HIGHLIGHT;
            }
            else
            {
                _gameState.CurrentSquare = null;
            }
        }

        private static void ResetCurrentSquare()
        {
            if (_gameState.CurrentSquare != null)
            {
                _gameState.CurrentSquare.Texture = GameConstants.redSquare;
            }
        }

        private static void LoadGameSettings(StreamReader reader)
        {
            string text = reader.ReadLine();
            _gameState.ExtraMove = text == GameConstants.HAD_COMBO.ToString();

            text = reader.ReadLine();
            _gameState.ExtraPath = text == GameConstants.EXTRA_PATH.ToString();
        }

        private static void LoadTurn(StreamReader reader)
        {
            string text = reader.ReadLine();
            if (text == GameConstants.RED_TURN.ToString())
            {
                _gameState.Turn.PlayerColor = PieceColor.Red;
                _gameState.Turn.TurnImage = GameConstants.redPiece;
            }
            else
            {
                _gameState.Turn.PlayerColor = PieceColor.White;
                _gameState.Turn.TurnImage = GameConstants.whitePiece;
            }
        }

        private static void LoadBoard(StreamReader reader, ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            for (int row = 0; row < GameConstants.BOARD_SIZE; row++)
            {
                string text = reader.ReadLine();
                for (int col = 0; col < GameConstants.BOARD_SIZE; col++)
                {
                    squares[row][col].LegalSquareSymbol = null;
                    squares[row][col].Piece = CreatePieceFromSymbol(text[col], squares[row][col]);
                }
            }
        }

        private static GamePiece CreatePieceFromSymbol(char symbol, GameSquare square)
        {
            return symbol switch
            {
                GameConstants.NO_PIECE => null,
                GameConstants.RED_PIECE => new GamePiece(PieceColor.Red, PieceType.Regular) { Square = square },
                GameConstants.RED_KING => new GamePiece(PieceColor.Red, PieceType.King) { Square = square },
                GameConstants.WHITE_PIECE => new GamePiece(PieceColor.White, PieceType.Regular) { Square = square },
                GameConstants.WHITE_KING => new GamePiece(PieceColor.White, PieceType.King) { Square = square },
                _ => throw new InvalidDataException($"Unknown piece symbol: {symbol}")
            };
        }

        private static void LoadCurrentNeighbours(StreamReader reader, ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            foreach (var square in _gameState.CurrentNeighbours.Keys)
            {
                square.LegalSquareSymbol = null;
            }

            _gameState.CurrentNeighbours.Clear();

            string text;
            while ((text = reader.ReadLine()) != "-")
            {
                if (text.Length == 1)
                {
                    break;
                }
                else if (text.Length == 2)
                {
                    _gameState.CurrentNeighbours.Add(squares[(int)char.GetNumericValue(text[0])][(int)char.GetNumericValue(text[1])], null);
                }
                else
                {
                    _gameState.CurrentNeighbours.Add(
                        squares[(int)char.GetNumericValue(text[0])][(int)char.GetNumericValue(text[1])],
                        squares[(int)char.GetNumericValue(text[2])][(int)char.GetNumericValue(text[3])]
                    );
                }
            }
        }
    }
}
