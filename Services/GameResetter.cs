﻿using Checkers.Enums;
using Checkers.Models;
using System.Collections.ObjectModel;

namespace Checkers.Services
{
    public class GameResetter
    {
        public static void ResetGame(ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            // Створюємо новий об'єкт стану гри
            GameState gameState = new GameState();

            // Очищаємо всі позначки на клітках, які можуть бути виділені
            foreach (var square in gameState.CurrentNeighbours.Keys)
            {
                square.LegalSquareSymbol = null;
            }

            // Якщо є виділена клітка, змінюємо її текстуру на початкову
            if (gameState.CurrentSquare != null)
            {
                gameState.CurrentSquare.Texture = GameConstants.redSquare;
            }

            // Очищаємо список сусідів і встановлюємо додаткові стани гри на початкові
            gameState.CurrentNeighbours.Clear();
            gameState.CurrentSquare = null;
            gameState.ExtraMove = false;
            gameState.ExtraPath = false;

            // Скидуємо лічильники зібраних фігур для обох гравців
            GameConstants.CollectedWhitePieces = 0;
            GameConstants.CollectedRedPieces = 0;

            // Встановлюємо чергу першого гравця (червоного)
            gameState.Turn.PlayerColor = PieceColor.Red;

            // Скидаємо конфігурацію дошки
            ResetGameBoard(squares);
        }

        private static void ResetGameBoard(ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            // Проходимо по кожній клітці на дошці
            for (int row = 0; row < GameConstants.BOARD_SIZE; row++)
            {
                for (int col = 0; col < GameConstants.BOARD_SIZE; col++)
                {
                    // Якщо клітина відповідає чорній клітині на дошці
                    if ((row + col) % 2 == 0)
                    {
                        // Знімаємо фігуру, якщо вона є
                        squares[row][col].Piece = null;
                    }
                    else if (row < 3) // Початкове розміщення фігур для білого гравця
                    {
                        squares[row][col].Piece = new GamePiece(PieceColor.White)
                        {
                            Square = squares[row][col]
                        };
                    }
                    else if (row > 4) // Початкове розміщення фігур для червоного гравця
                    {
                        squares[row][col].Piece = new GamePiece(PieceColor.Red)
                        {
                            Square = squares[row][col]
                        };
                    }
                    else // Очищення решти клітинок
                    {
                        squares[row][col].Piece = null;
                    }
                }
            }
        }
    }
}
