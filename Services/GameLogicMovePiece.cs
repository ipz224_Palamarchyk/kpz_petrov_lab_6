﻿using Checkers.Enums;
using Checkers.Models;
using System.Collections.ObjectModel;

namespace Checkers.Services;

public partial class GameLogicMovePiece : GameLogicBase
{
    public GameLogicMovePiece()
    {
    }

    public GameLogicMovePiece(ObservableCollection<ObservableCollection<GameSquare>> board, PlayerTurn playerTurn, Winner winner) : base(board, playerTurn, winner)
    {
    }

    public void MovePiece(GameSquare square)
    {
        square.Piece = Utility.CurrentSquare.Piece;
        square.Piece.Square = square;

        if (Utility.CurrentNeighbours[square] != null)
        {
            Utility.CurrentNeighbours[square].Piece = null;
            Utility.ExtraMove = true;
        }
        else
        {
            Utility.ExtraMove = false;
            SwitchTurns(Utility.CurrentSquare);
        }

        board[Utility.CurrentSquare.Row][Utility.CurrentSquare.Column].Texture = Utility.redSquare;

        foreach (GameSquare selectedSquare in Utility.CurrentNeighbours.Keys)
        {
            selectedSquare.LegalSquareSymbol = null;
        }
        Utility.CurrentNeighbours.Clear();
        Utility.CurrentSquare.Piece = null;
        Utility.CurrentSquare = null;

        if (square.Piece.Type == PieceType.Regular)
        {
            if (square.Row == 0 && square.Piece.Color == PieceColor.Red)
            {
                square.Piece.Type = PieceType.King;
                square.Piece.Texture = Utility.redKingPiece;
            }
            else if (square.Row == board.Count - 1 && square.Piece.Color == PieceColor.White)
            {
                square.Piece.Type = PieceType.King;
                square.Piece.Texture = Utility.whiteKingPiece;
            }
        }

        if (Utility.ExtraMove)
        {
            if (playerTurn.TurnImage == Utility.redPiece)
            {
                Utility.CollectedWhitePieces++;
            }
            if (playerTurn.TurnImage == Utility.whitePiece)
            {
                Utility.CollectedRedPieces++;
            }
            DisplayRegularMoves(square);
        }

        if (Utility.CollectedRedPieces == 12 || Utility.CollectedWhitePieces == 12)
        {
            GameOver();
        }
    }
}
