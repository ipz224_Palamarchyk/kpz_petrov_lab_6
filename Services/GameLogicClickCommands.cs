﻿using Checkers.Enums;
using Checkers.Models;
using System.Collections.ObjectModel;

namespace Checkers.Services
{
    public partial class GameLogicClickCommands : GameLogicBase
    {
        private GameController gameController;

        public GameLogicClickCommands(ObservableCollection<ObservableCollection<GameSquare>> board, PlayerTurn playerTurn, Winner winner) : base(board, playerTurn, winner)
        {
            gameController = new GameController(board);
        }

        public void ResetGame()
        {
            gameController.ResetGame();
        }

        public void SaveGame()
        {
            gameController.SaveGameToFile();
        }

        public void LoadGame()
        {
            gameController.LoadGameFromFile();
            playerTurn.TurnImage = Utility.Turn.TurnImage;
        }

        public void ClickPiece(GameSquare square)
        {
            if ((Utility.Turn.PlayerColor == square.Piece.Color) && !Utility.ExtraMove)
            {
                DisplayRegularMoves(square);
            }
        }
    }
}
