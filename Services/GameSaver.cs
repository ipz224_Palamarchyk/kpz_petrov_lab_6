﻿using Checkers.Enums;
using Checkers.Models;
using Microsoft.Win32;
using System.Collections.ObjectModel;
using System.IO;


namespace Checkers.Services
{
    public static class GameSaver
    {
        private static GameState _gameState;

        public static void SaveGame(ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            _gameState = new GameState();

            SaveFileDialog saveDialog = new SaveFileDialog();
            bool? answer = saveDialog.ShowDialog();
            if (answer == true)
            {
                var path = saveDialog.FileName;
                using (var writer = new StreamWriter(path))
                {
                    SaveCurrentSquare(writer);
                    SaveGameState(writer);
                    SaveTurn(writer);
                    SaveBoard(writer, squares);
                    SaveCurrentNeighbours(writer);
                }
            }
        }

        private static void SaveCurrentSquare(StreamWriter writer)
        {
            if (_gameState.CurrentSquare != null)
            {
                writer.Write(_gameState.CurrentSquare.Row.ToString() + _gameState.CurrentSquare.Column.ToString());
            }
            else
            {
                writer.Write(GameConstants.NO_PIECE);
            }
            writer.WriteLine();
        }

        private static void SaveGameState(StreamWriter writer)
        {
            if (_gameState.ExtraMove)
            {
                writer.Write(GameConstants.HAD_COMBO);
            }
            else
            {
                writer.Write(GameConstants.NO_PIECE);
            }
            writer.WriteLine();

            if (_gameState.ExtraPath)
            {
                writer.Write(GameConstants.EXTRA_PATH);
            }
            else
            {
                writer.Write(GameConstants.NO_PIECE);
            }
            writer.WriteLine();
        }

        private static void SaveTurn(StreamWriter writer)
        {
            if (_gameState.Turn.PlayerColor.Equals(PieceColor.Red))
            {
                writer.Write(GameConstants.RED_TURN);
            }
            else
            {
                writer.Write(GameConstants.WHITE_TURN);
            }
            writer.WriteLine();
        }

        private static void SaveBoard(StreamWriter writer, ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            foreach (var line in squares)
            {
                foreach (var square in line)
                {
                    writer.Write(GetSymbolFromPiece(square.Piece));
                }
                writer.WriteLine();
            }
        }

        private static char GetSymbolFromPiece(GamePiece piece)
        {
            if (piece == null) return GameConstants.NO_PIECE;
            return piece.Color switch
            {
                PieceColor.Red when piece.Type == PieceType.Regular => GameConstants.RED_PIECE,
                PieceColor.White when piece.Type == PieceType.Regular => GameConstants.WHITE_PIECE,
                PieceColor.White when piece.Type == PieceType.King => GameConstants.WHITE_KING,
                PieceColor.Red when piece.Type == PieceType.King => GameConstants.RED_KING,
                _ => throw new InvalidDataException($"Unknown piece type: {piece.Type}")
            };
        }

        private static void SaveCurrentNeighbours(StreamWriter writer)
        {
            foreach (var square in _gameState.CurrentNeighbours.Keys)
            {
                if (_gameState.CurrentNeighbours[square] == null)
                {
                    writer.Write(square.Row.ToString() + square.Column.ToString() + GameConstants.NO_PIECE);
                }
                else
                {
                    writer.Write(square.Row.ToString() + square.Column.ToString() + _gameState.CurrentNeighbours[square].Row.ToString() + _gameState.CurrentNeighbours[square].Column.ToString());
                }
                writer.WriteLine();
            }
            writer.Write("-\n");
        }
    }
}

