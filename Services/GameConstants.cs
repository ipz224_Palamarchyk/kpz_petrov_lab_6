﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Services
{
    public static class GameConstants
    {
        public const char NO_PIECE = 'N';
        public const char WHITE_PIECE = 'W';
        public const char RED_PIECE = 'R';
        public const char RED_KING = 'K';
        public const char WHITE_KING = 'L';
        public const char WHITE_TURN = '2';
        public const char RED_TURN = '1';
        public const char HAD_COMBO = 'H';
        public const char EXTRA_PATH = 'E';
        public const int BOARD_SIZE = 8;
        public const string redSquare = "/Checkers;component/Resources/squareRed.png";
        public const string whiteSquare = "/Checkers;component/Resources/squareWhite.png";
        public const string redPiece = "/Checkers;component/Resources/pieceRed.png";
        public const string whitePiece = "/Checkers;component/Resources/pieceWhite.png";
        public const string hintSquare = "/Checkers;component/Resources/squareHint.png";
        public const string redKingPiece = "/Checkers;component/Resources/pieceRedKing.png";
        public const string whiteKingPiece = "/Checkers;component/Resources/pieceWhiteKing.png";
        public const string SQUARE_HIGHLIGHT = "NULL";
        public static int CollectedRedPieces = 0;
        public static int CollectedWhitePieces = 0;
    }
}
